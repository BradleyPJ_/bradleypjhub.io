---
layout: page
title: About
permalink: /about/
---

I'm a Junior Web Developer, Training at [Yet2Learn](http://yet2learn.org.uk/). Yet2Learn is Youth Enterprise Training with a group of people dedicated in giving students the skills they need to succeed in the real world.

My Personal Site is at [BradleyPJ.com](http://bradleypj.com)