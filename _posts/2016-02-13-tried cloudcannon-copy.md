---
layout: post
title:  "Tried CloudCannon"
date:   2016-02-13 11:20 +0000
categories: Blog
---

While exploring [Jekyll](https://jekyllrb.com/)&nbsp;and looking online at themes and other related tools, i came across [CloudCannon](http://cloudcannon.com/), which should act as a cms for [Jeklly](https://jekyllrb.com/)&nbsp;or more so a visual editor.

They have a subscription but i plan on working around that using [github pages](https://pages.github.com/) to host the pages and sync them back and forth.

–EDIT

By using a [github pages](https://pages.github.com/) repo i was able to get around the need for a plan.&nbsp;