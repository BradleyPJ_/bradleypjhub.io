---
layout: post
title:  "Bootstrap Flex Gallery"
date:   2016-02-16 11:20 +0000
categories: Blog
---

Updated the [Yet2Learn](http://yet2learn.org.uk/index.html) site, it now has a photo gallery using flexbox the fine details of the project actually  took more time than expected.
And it kind of works on mobile.

Likes:
* Flex Box Adjust to number of images
* Overlay works and takes the src of the clicked image
* Works on mobile

Dislikes:
* Close button in the Overlay
* Image Crop in Gallery
* Scrolling fast in the Overlay causes stuttering in the background

Setting up a github account for Yet2Learn that I will soon be able to upload to and be a Contributor. 
