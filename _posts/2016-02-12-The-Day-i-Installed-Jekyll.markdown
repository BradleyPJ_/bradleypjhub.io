---
layout: post
title:  "The Day i Installed Jekyll"
date:   2016-02-12 19:20:57 +0000
categories: Blog
---

[`Jekyll`](https://jekyllrb.com/) is static site generator. This means you pass it files such as blog posts and it generates a static site. its fully customization and is even supported by githubs pages (where i'm hosting this)

Today i installed [`Jekyll`](https://jekyllrb.com/), which is something i wanted to do along time ago but was held back by some ssl errors.

`SSL_connect returned=1 errno=0 state=SSLv3 read server certificate B: certificate verify failed`

I was able to use this [Tutorial](https://gist.github.com/luislavena/f064211759ee0f806c88) to solve the issue today. Came across the same error a couple of days ago while learning ruby on rails.

Overall this gem looks promising and i'm looking forward to getting to know it more as i develop this blog.